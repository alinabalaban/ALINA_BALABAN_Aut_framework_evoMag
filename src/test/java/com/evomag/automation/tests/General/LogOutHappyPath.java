package com.evomag.automation.tests.General;

import com.evomag.automation.BaseTest;
import com.evomag.automation.pagesDefinition.FirstPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import static com.evomag.automation.utils.TestUtils.mySleeper;

public class LogOutHappyPath extends BaseTest {
    @Test
    public void LogOut() {
        mySleeper();
        FirstPage FirstPageUsage = PageFactory.initElements(driver, FirstPage.class);
        FirstPage.Logout(driver);
    }
}
