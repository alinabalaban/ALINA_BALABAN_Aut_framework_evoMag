package com.evomag.automation.tests.General;

import com.evomag.automation.BaseTest;
import com.evomag.automation.pagesDefinition.CampaignPage;
import com.evomag.automation.pagesDefinition.FirstPage;
import com.evomag.automation.pagesDefinition.LoginPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class LoginHappyPath extends BaseTest {
    @Test
    public void login() {
        CampaignPage CampainUsage = PageFactory.initElements(driver, CampaignPage.class);
        CampainUsage.BacktoSitePush();
        FirstPage FirstPageUsage = PageFactory.initElements(driver, FirstPage.class);
        FirstPage.pushLoginButtons();
        LoginPage LoginPageUsage = PageFactory.initElements(driver, LoginPage.class);
        LoginPage.LoginWithUser("abalaban@oberospm.com", "333333");
        CampainUsage.BacktoSitePush();
    }
}
