package com.evomag.automation.tests.Logged;


import com.evomag.automation.BaseTest;
import com.evomag.automation.pagesDefinition.*;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.evomag.automation.utils.TestUtils.mySleeper;

public class TestAddToWishList extends BaseTest {
    @Test(groups = {"Loggedin"})
    public void TestWishList() {
        CampaignPage CampainUsage = PageFactory.initElements(driver, CampaignPage.class);
        CampainUsage.BacktoSitePush();
        SearchPage SearchPageUsage = PageFactory.initElements(driver, SearchPage.class);
        mySleeper();
        SearchPageUsage.search("Telefon mobil");
        mySleeper(1000);
        AllProductsPage AllProductsPageUSage = PageFactory.initElements(driver, AllProductsPage.class);
        AllProductsPageUSage.clickOnRandElement();
        ProductPage ProductPageUsage = PageFactory.initElements(driver, ProductPage.class);
        ProductPageUsage.PageProductValidation();
        String ProductNameFirst = ProductPage.Productname.getText();
        mySleeper(1000);
        ProductPageUsage.AddToWishList();
        WishListPage WishListPageUSage = PageFactory.initElements(driver, WishListPage.class);
        WishListPageUSage.addWishlist();
        mySleeper(1000);
        Assert.assertEquals(ProductNameFirst, WishListPageUSage.getElementNameW());
        WishListPageUSage.deleteWishList();

    }


}

