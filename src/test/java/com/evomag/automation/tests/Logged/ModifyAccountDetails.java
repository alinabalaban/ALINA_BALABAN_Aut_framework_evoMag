package com.evomag.automation.tests.Logged;

import com.evomag.automation.BaseTest;
import com.evomag.automation.pagesDefinition.AccountDetailsPage;
import com.evomag.automation.pagesDefinition.CampaignPage;
import com.evomag.automation.pagesDefinition.SearchPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import static com.evomag.automation.utils.TestUtils.mySleeper;

public class ModifyAccountDetails extends BaseTest {
    @Test(groups = {"LoggedIn"})
    public void ModifyAccountDetails() {
        CampaignPage CampainUsage = PageFactory.initElements(driver, CampaignPage.class);
        CampainUsage.BacktoSitePush();
        SearchPage SearchPageUsage = PageFactory.initElements(driver, SearchPage.class);
        mySleeper();
        SearchPageUsage.UserElemetHovered();
        AccountDetailsPage AccountDetailsPageUsage = PageFactory.initElements(driver, AccountDetailsPage.class);
        AccountDetailsPageUsage.verifyDetails();
    }
}
