package com.evomag.automation.tests.Logged;


import com.evomag.automation.BaseTest;
import com.evomag.automation.pagesDefinition.*;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import static com.evomag.automation.utils.TestUtils.mySleeper;

//This test is login in, search a product, open a random product, add it to the cart, check the confirmation screen, then logout
public class TestBuyElement extends BaseTest {
    @Test(groups = {"Loggedin"})
    public void TestBuyElement() {

        CampaignPage CampainUsage = PageFactory.initElements(driver, CampaignPage.class);
        CampainUsage.BacktoSitePush();
        SearchPage SearchPageUsage = PageFactory.initElements(driver, SearchPage.class);
        mySleeper();
        SearchPageUsage.search("laptop dell");
        mySleeper(1000);
        AllProductsPage AllProductsPageUSage = PageFactory.initElements(driver, AllProductsPage.class);
        AllProductsPageUSage.clickOnRandElement();
        ProductPage ProductPageUsage = PageFactory.initElements(driver, ProductPage.class);
        ProductPageUsage.PageProductValidation();
        ProductPageUsage.AddToCartButton();
        PopupAddedCart PopupAddedCartUsage = PageFactory.initElements(driver, PopupAddedCart.class);
        PopupAddedCartUsage.ConfirmationCart();
        PopupAddedCartUsage.CheckCartDetails();
        CartPage CartPageUsage = PageFactory.initElements(driver, CartPage.class);
        CartPageUsage.DeleteElements();


    }
}
