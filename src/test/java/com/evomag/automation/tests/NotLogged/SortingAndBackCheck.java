package com.evomag.automation.tests.NotLogged;

import com.evomag.automation.BaseTest;
import com.evomag.automation.pagesDefinition.AllProductsPage;
import com.evomag.automation.pagesDefinition.CampaignPage;
import com.evomag.automation.pagesDefinition.SearchPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class SortingAndBackCheck extends BaseTest {
    @Test(groups = {"Delogged"})
    public void TestSorting() {
        CampaignPage CampainUsage = PageFactory.initElements(driver, CampaignPage.class);
        CampainUsage.BacktoSitePush();
        SearchPage SearchPageUsage = PageFactory.initElements(driver, SearchPage.class);
        SearchPageUsage.search("laptop");
        AllProductsPage AllProductsPageUsage = PageFactory.initElements(driver, AllProductsPage.class);
        AllProductsPageUsage.checkProductsPage();
        AllProductsPageUsage.sortAscending();
        AllProductsPageUsage.checkSort("ASC");
        AllProductsPageUsage.clickOnRandElement();
        pressBack();
        AllProductsPageUsage.sortDescending();
        AllProductsPageUsage.checkSort("DESC");
        AllProductsPageUsage.clickOnRandElement();
        pressBack();

    }
}
