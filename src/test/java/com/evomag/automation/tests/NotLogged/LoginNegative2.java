package com.evomag.automation.tests.NotLogged;

import com.evomag.automation.BaseTest;
import com.evomag.automation.pagesDefinition.CampaignPage;
import com.evomag.automation.pagesDefinition.FirstPage;
import com.evomag.automation.pagesDefinition.LoginPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class LoginNegative2 extends BaseTest {

    @Test
    public void LoginWithoutUser()

    {
        driver.get("https://www.evomag.ro/frontendCampaign/index");
        CampaignPage CampainUsage = PageFactory.initElements(driver, CampaignPage.class);
        CampainUsage.BacktoSitePush();
        FirstPage FirstPageUSage = PageFactory.initElements(driver, FirstPage.class);

        FirstPage.pushLoginButtons();
        LoginPage LoginPageUsage = PageFactory.initElements(driver, LoginPage.class);
        LoginPage.LoginWithUser("", "AA33333");
        LoginPageUsage.ValidateErrorNoUserPass();
    }
}
