package com.evomag.automation.tests.NotLogged;

import com.evomag.automation.BaseTest;
import com.evomag.automation.pagesDefinition.CampaignPage;
import com.evomag.automation.pagesDefinition.IphonesPage;
import com.evomag.automation.pagesDefinition.SearchPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class IphoneTestDB extends BaseTest {
    @Test(groups = {"Delogged"})
    public void LoginWrongUser() {
        CampaignPage CampainUsage = PageFactory.initElements(driver, CampaignPage.class);
        CampainUsage.BacktoSitePush();
        SearchPage SearchPageUsage = PageFactory.initElements(driver, SearchPage.class);
        SearchPageUsage.ClickOnIphone();
        IphonesPage page5 = PageFactory.initElements(driver, IphonesPage.class);
        page5.ValidateIphonePage();
        page5.DatabaseConnection();
        page5.validateDB();

    }
}
