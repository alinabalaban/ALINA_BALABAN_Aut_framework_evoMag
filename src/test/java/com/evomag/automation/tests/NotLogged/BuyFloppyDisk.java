package com.evomag.automation.tests.NotLogged;

import com.evomag.automation.BaseTest;
import com.evomag.automation.pagesDefinition.AllProductsPage;
import com.evomag.automation.pagesDefinition.CampaignPage;
import com.evomag.automation.pagesDefinition.SearchPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import static com.evomag.automation.utils.TestUtils.mySleeper;

public class BuyFloppyDisk extends BaseTest {
    ///This test case contains navigation throw page, by hovering elements, then choose one, and try to buy an element with no stock
    @Test(groups = {"Delogged"})
    public void BuyFloppy() {
        CampaignPage CampainUsage = PageFactory.initElements(driver, CampaignPage.class);
        CampainUsage.BacktoSitePush();
        SearchPage SearchPageUsage = PageFactory.initElements(driver, SearchPage.class);
        mySleeper();
        SearchPageUsage.ChooseElemetHovered();
        AllProductsPage AllProductsPageUsage = PageFactory.initElements(driver, AllProductsPage.class);
        AllProductsPageUsage.clickOnRandElement();

    }
}
