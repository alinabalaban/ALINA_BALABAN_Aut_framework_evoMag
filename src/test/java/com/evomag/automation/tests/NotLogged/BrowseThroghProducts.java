package com.evomag.automation.tests.NotLogged;

import com.evomag.automation.BaseTest;
import com.evomag.automation.pagesDefinition.*;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import static com.evomag.automation.utils.TestUtils.mySleeper;

public class BrowseThroghProducts extends BaseTest {
    @Test(groups = {"Delogged"})
            public void browseThroughProducts(){
        CampaignPage CampainUsage = PageFactory.initElements(driver, CampaignPage.class);
        CampainUsage.BacktoSitePush();
        SearchPage SearchPageUsage = PageFactory.initElements(driver, SearchPage.class);
        mySleeper();
        SearchPageUsage.search("calculator");
        mySleeper(1000);
        AllProductsPage AllProductsPageUsage = PageFactory.initElements(driver, AllProductsPage.class);
        AllProductsPageUsage.clickOnRandElement();
        ProductPage ProductPageUsage = PageFactory.initElements(driver, ProductPage.class);
        ProductPageUsage.chooseTab();
        mySleeper(1000);
        TabsContainingPage TabsContainingPageUsage = PageFactory.initElements(driver, TabsContainingPage.class);
//        TabsContainingPageUsage.pushArrows();
        pressBack();
        AllProductsPageUsage.clickOnAllElements();


    }
}
