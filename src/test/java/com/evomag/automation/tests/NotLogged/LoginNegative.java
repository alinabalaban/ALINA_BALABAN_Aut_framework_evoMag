package com.evomag.automation.tests.NotLogged;

import com.evomag.automation.BaseTest;
import com.evomag.automation.pagesDefinition.CampaignPage;
import com.evomag.automation.pagesDefinition.FirstPage;
import com.evomag.automation.pagesDefinition.LoginPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class LoginNegative extends BaseTest {
    @Test()
    public void LoginWrongUser() {
        CampaignPage CampainUsage = PageFactory.initElements(driver, CampaignPage.class);
        CampainUsage.BacktoSitePush();
        FirstPage FirstPageUsage = PageFactory.initElements(driver, FirstPage.class);
        FirstPage.pushLoginButtons();
        LoginPage LoginPageUsage = PageFactory.initElements(driver, LoginPage.class);
        LoginPage.LoginWithUser("abalaban@oberospm.com", "AA33333");
        LoginPageUsage.ValidateErrorWrongUser();

    }


}
