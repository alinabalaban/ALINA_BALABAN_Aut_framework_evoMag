package com.evomag.automation.tests.NotLogged;


import com.evomag.automation.BaseTest;
import com.evomag.automation.pagesDefinition.AllProductsPage;
import com.evomag.automation.pagesDefinition.CampaignPage;
import com.evomag.automation.pagesDefinition.SearchPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import static com.evomag.automation.utils.TestUtils.mySleeper;

public class TestSearchImposibleElement extends BaseTest {
    @Test(groups = {"Loggedin"})
    public void NegativTestingSearch() {
        CampaignPage CampainUsage = PageFactory.initElements(driver, CampaignPage.class);
        CampainUsage.BacktoSitePush();
        SearchPage SearchPageUsage = PageFactory.initElements(driver, SearchPage.class);
        mySleeper(1000);
        SearchPageUsage.search("Covrigi123456987");
        mySleeper(1000);
        AllProductsPage AllProductsPageUSage = PageFactory.initElements(driver, AllProductsPage.class);
        AllProductsPageUSage.clickOnRandElement();
    }
}
