package com.evomag.automation.tests.NotLogged;

import com.evomag.automation.BaseTest;
import com.evomag.automation.pagesDefinition.CampaignPage;
import com.evomag.automation.pagesDefinition.FiltersPage;
import com.evomag.automation.pagesDefinition.SearchPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import static com.evomag.automation.utils.TestUtils.mySleeper;

public class DataFiltering extends BaseTest {
    @Test(groups = {"Delogged"})
    public void VerifyFilters() {
        CampaignPage CampainUsage = PageFactory.initElements(driver, CampaignPage.class);
        CampainUsage.BacktoSitePush();
        SearchPage SearchPageUsage = PageFactory.initElements(driver, SearchPage.class);
        mySleeper();
        SearchPageUsage.search("laptop dell");
        mySleeper(1000);
        FiltersPage FiltersPageUsage = PageFactory.initElements(driver, FiltersPage.class);
        FiltersPageUsage.setPriceSlider();
        FiltersPageUsage.selectFilters();
        FiltersPageUsage.deleteFilters();


    }
}
