package com.evomag.automation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class BaseTest {

//    public static void DatabaseConnection(String username, String password, String database) {
//        System.out.println("Login with user" +" on DB "+ database);
//        try {
//            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/" +database+ "?useSSL=false&serverTimezone=UTC", username, password);
//            Statement stm = conn.createStatement();
//
//        }
//        catch (SQLException sexc) {
//            sexc.printStackTrace();
//        }
//
//
//    }
    public static WebDriver driver;
 public static void pressBack(){
     driver.navigate().back();
 }

    @BeforeTest()
    public static void startDriver() {
        System.setProperty("webdriver.chrome.driver", "src\\main\\Drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.evomag.ro/frontendCampaign/index");
    }

    @AfterTest
    public static void stopDriver() {
        driver.quit();
    }




}
