package com.evomag.automation.pagesDefinition;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.util.List;
import java.util.Random;

import static com.evomag.automation.BaseTest.driver;
import static com.evomag.automation.utils.TestUtils.mySleeper;

public class ProductPage {
    Random rand = new Random();

    ///Add to wishlist element should be present in every product page
    @FindBy(how = How.XPATH, using = ".//*[@id='personal_header']/div[2]/div[1]/em")
    public static WebElement isLogged;

    //Add to cart button should be present if the product is available
    @FindBy(how = How.XPATH, using = "//A[@class='cart'][text()='Adauga in cos']")
    private static WebElement addToCart;

    @FindBy(how = How.XPATH, using = "//A[@class='ordinary-a add_to_wish_button'][text()='Adauga la wishlist']")
    private static WebElement addToWishList;
    @FindBy(how = How.XPATH, using = ".//*[@id='add-to-cart-detail']/div/div[1]/h1[1]")
    public static WebElement Productname;

    @FindBy(how = How.XPATH, using = "//A[@class='cart'][text()='Adauga in cos']")
    private static WebElement AddToCartButton;
    public String ProductName1;

    @FindBy(how = How.XPATH, using = ".//*[@id='ConsumabileAccesorii']/ul/li[1]")
    private static WebElement Mouse;

    @FindBy(how = How.XPATH, using = ".//*[@id='ConsumabileAccesorii']/ul/li[3]")
    private static WebElement Tab1;

    @FindAll({@FindBy(how = How.XPATH, using = ".//*[@id='ConsumabileAccesorii']/ul/li")})
    private List<WebElement> RecomandedProductsTabs;

    @FindAll({@FindBy(how = How.XPATH, using = "//EM[text()='Wishlist']")})
    private static WebElement WishlistConfirmation;

    @FindAll({@FindBy(how = How.XPATH, using = "//DIV[@id='CrossSaleModal']")})
    private static WebElement ModalConfirmation;




    /**
     * This method verifies if there are tabs on suggested products
     * if not returns "No tabs to be pushed"
     * else clicks on random tab
     */

    public void chooseTab(){
            if (RecomandedProductsTabs.size() == 0) {
                System.out.println("No tabs to be pushed");
            } else {
                    JavascriptExecutor jse = (JavascriptExecutor)driver;
                    jse.executeScript("scroll(0, 450);");
                    mySleeper(1000);
                    int max = RecomandedProductsTabs.size() - 1;
                    int min = 0;
                    int randomNum = rand.nextInt((max - min) + 1) + min;
                    mySleeper(1000);
                        RecomandedProductsTabs.get(randomNum).click();
            }
        }

    /**
     * This method pushes AddToCartButton and verifies that the modal is displayed - IphonesPage.ValidateIphonePage()
     */
    public void AddToCartButton() {
        mySleeper(1000);
        AddToCartButton.click();
        mySleeper(200);
        Assert.assertTrue(ModalConfirmation.isDisplayed());
    }

    /**
     * This method Return the name of product AddToCartButton and it will be used in test for asserts
     */
    public String getName() {
        ProductName1 = Productname.getText();
        return ProductName1;
    }

    /**
     * This method asure that the user logged in, is the right user
     */
    public void PageProductValidation() {
        addToWishList.isDisplayed();
        Assert.assertEquals(isLogged.getText(), "Alina B.");

    }

    /**
     * This method clicks on add to wishlist and validates the menu you are in
     */
    public void AddToWishList() {

        addToWishList.click();
        Assert.assertEquals(WishlistConfirmation.getText(), "Wishlist");
//        System.out.println( "WishlistConfirmation"+WishlistConfirmation.getText());
    }


}
