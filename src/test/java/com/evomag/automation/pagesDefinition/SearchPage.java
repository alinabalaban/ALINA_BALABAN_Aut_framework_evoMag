package com.evomag.automation.pagesDefinition;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import static com.evomag.automation.BaseTest.driver;
import static com.evomag.automation.utils.TestUtils.mySleeper;

public class SearchPage {
    @FindBy(how = How.XPATH, using = "//INPUT[@id='searchString'][2]")
    public static WebElement SearchBox;

    @FindBy(how = How.XPATH, using = ".//*[@id='menu_catex']/div[2]/div/ul/li[4]/a")
    public static WebElement PcComponets;

    @FindBy(how = How.XPATH, using = "//A[@href='/Componente-PC-Floppy-Disk/'][text()='Floppy Disk (2)']")
    public static WebElement FloppyDisk;

    @FindBy(how = How.XPATH, using = "(//DIV[@class='c_header'])[2]")
    public static WebElement UserLogedin;

    @FindBy(how = How.XPATH, using = "(//DIV[@class='c_header'])[1]")
    public static WebElement UserLogedin1;

    @FindBy(how = How.XPATH, using = "//A[@rel='nofollow'][text()='Detalii cont'][1]")
    public static WebElement Accountdetails;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[3]/div[2]/div/ul/li[1]/a")
    public static WebElement IphoneTab;

    @FindBy(how = How.XPATH, using = "//INPUT[@type='submit']")
    private static WebElement SubmitSearch;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div/h1")
    private static WebElement Infosearch;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div/div[2]/div[2]/h3/div/div")
    private static WebElement InfosearchNegative;


    /**
     * This method clicks on tab Iphone, there is other method that verifies that the page was opened
     */
    public void ClickOnIphone() {
        IphoneTab.isDisplayed();
        IphoneTab.click();
    }

    /**
     * This method search products by string S, and validate that the search was performed
     */
    public void search(String S) {

//        System.out.println("[DEBUG] : " + SearchBox.isDisplayed());

        SearchBox.click();
        SearchBox.clear();
        SearchBox.sendKeys(S);
        Assert.assertEquals(SearchBox.getAttribute("value"), S);
        SubmitSearch.click();
        mySleeper(1000);
        try {
            Assert.assertTrue(Infosearch.isDisplayed() == true);
            Assert.assertTrue(Infosearch.isDisplayed());
//            System.out.println("Infosearch" + Infosearch.getText());
            mySleeper(1000);
            Assert.assertEquals(Infosearch.getText(), "Rezultate cautare pentru '" + S + "'");
            mySleeper(1000);
        } catch (NoSuchElementException E) {
//            System.out.println();
            mySleeper(1000);
            Assert.assertTrue(InfosearchNegative.isDisplayed());
            Assert.assertEquals(InfosearchNegative.getText(), "CRITERIILE DE FILTRARE SELECTATE DE DUMNEAVOASTRA NU AU RETURNAT NICI UN REZULTAT!");
            mySleeper(1000);

        }
    }

    /**
     * This hovers over the Pc components and choose floppy
     * Validates that the name of every  element is correct
     */
    public void ChooseElemetHovered() {
        Actions actions = new Actions(driver);
        actions = actions.moveToElement(PcComponets);
        WebDriverWait wait = new WebDriverWait(driver, 10);
//        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated( By.partialLinkText("Componente PC")));
//        Assert.assertEquals(PcComponets.getText(), "Componente PC");
        mySleeper(1000);
        Action action = actions.build();
        action.perform();
        String PartialName = FloppyDisk.getText().substring(0, FloppyDisk.getText().indexOf(" "));
        Assert.assertEquals(PartialName, "Floppy");
        FloppyDisk.click();
    }

    /**
     * This hovers over the Account logged in, and click on account details
     * Validates that the name of the hovered element and account details is correct
     * Next Assert in on @see method verifyDetails();
     */
    public void UserElemetHovered() {
        Actions actions = new Actions(driver);
        actions = actions.moveToElement(UserLogedin);
        Assert.assertEquals(UserLogedin.getText(), "Bine ai venit\n" + "Alina B.");
        Action action = actions.build();
        action.perform();
        Assert.assertEquals(Accountdetails.getText(), "Detalii cont");
        Accountdetails.click();

    }
}
