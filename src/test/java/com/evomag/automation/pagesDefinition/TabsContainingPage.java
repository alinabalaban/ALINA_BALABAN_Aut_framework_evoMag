package com.evomag.automation.pagesDefinition;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.util.List;
import java.util.Random;

import static com.evomag.automation.BaseTest.driver;
import static com.evomag.automation.utils.TestUtils.mySleeper;

public class TabsContainingPage {
    Random rand = new Random();

    @FindBy(how = How.XPATH, using = "//A[@class='bx-prev']")
    private static WebElement ArrowLeft;

    @FindBy(how = How.XPATH, using = "(//A[@class='bx-next'])[1]")
    private static WebElement ArrowRight;

    @FindAll({@FindBy(how = How.CLASS_NAME, using = "npi_cartbutton")})
    public List<WebElement> ProductsSugested;

    /**
     * This method push the arrows left and right to navigate throw  list
     * Depends on page sometimes works, and sometimes it doesn't
     */
    public void pushArrows() {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        mySleeper(1000);
        Assert.assertTrue(ArrowRight.isDisplayed());
        ArrowRight.click();
        mySleeper(1000);
//        Assert.assertTrue(ArrowLeft.isDisplayed());
//        ArrowLeft.click();
    }

    /**
     * This method choose one random tab from suggested Elements
     */
    public void chooseTabElementFromTab(){
        if (ProductsSugested.size() == 0) {
            System.out.println("Search returned no element");
        } else {
            do {
                JavascriptExecutor jse = (JavascriptExecutor)driver;
                jse.executeScript("scroll(0, 950);");
                mySleeper(1000);
                int max = ProductsSugested.size() - 1;
                int min = 0;
                int randomNum = rand.nextInt((max - min) + 1) + min;
                mySleeper(1000);
                ProductsSugested.get(randomNum).click();
                break;
            }
            while (true);

        }
    }
}
