package com.evomag.automation.pagesDefinition;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.util.List;

import static com.evomag.automation.BaseTest.driver;
import static com.evomag.automation.utils.TestUtils.mySleeper;

public class WishListPage {

    @FindBy(how = How.XPATH, using = ".//*[@id='Wishlist_Name'][1]")
    private static WebElement NewWishListName;
    @FindBy(how = How.XPATH, using = ".//*[@id='add-wishlist']/div/table/tbody/tr[1]/td[2]/span[1]/a")
    private static WebElement NameElement;

    @FindBy(how = How.XPATH, using = ".//*[@id='wl-new']")
    private static WebElement NewWishList;
    @FindBy(how = How.XPATH, using = ".//*[@id='add-wishlist']/div/h1")
    private static WebElement NewWishlistDetails;

    @FindBy(how = How.XPATH, using = ".//*[@id='Wishlist_Comment']")
    private static WebElement NewWishListObservations;

    @FindBy(how = How.XPATH, using = "(//LI[text()='Detalii cont'])[1]")
    private static WebElement AccountDetails;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div[1]/div[2]/ul/li[5]")
    private static WebElement WishListTab;

    @FindBy(how = How.XPATH, using = "(//IMG[@src='/assets/ea939b4b/csgridview/delete.png'])[1]")
    private static WebElement DeleteWishList;
    @FindBy(how = How.XPATH, using = ".//*[@id='yw0_c0']")
    private static WebElement NewWishlistTable;

    @FindBy(how = How.XPATH, using = ".//*[@id='add-wishlist']/div[3]/div/input")
    private static WebElement NewWishListSave;

    @FindBy(how = How.XPATH, using = ".//*[@id='add-wishlist']/div[1]/div[1]/div[1]/label[1]")
    private static WebElement WishListLabel;
    String ProductNameW;
    @FindAll({@FindBy(how = How.XPATH, using = ".//*[@id='ConsumabileAccesorii']/ul/li")})
    private List<WebElement> RecomandedProductsTabs;

    /**
     * This method creates a wishlist
     * Confirms any text added is correct
     * Chek Confirmation text
     */
    public void addWishlist() {
        NewWishList.click();
        Assert.assertEquals(WishListLabel.getText(), "Denumire *");
        NewWishListName.click();
        NewWishListName.clear();
        NewWishListName.sendKeys("WishListNou");
        Assert.assertEquals(NewWishListName.getAttribute("value"), "WishListNou");
        NewWishListObservations.click();
        NewWishListObservations.clear();
        NewWishListObservations.sendKeys("Observatii");
        Assert.assertEquals(NewWishListObservations.getAttribute("value"), "Observatii");
        NewWishListSave.click();
        String DetailsWConf = NewWishlistDetails.getText().substring(0, NewWishlistDetails.getText().indexOf("-"));
        Assert.assertEquals(DetailsWConf, "Detalii wishlist ");

    }

    /**
     * This method deteles wishlist
     * Because one of their defects, can't click WishListTab fromthe first attempt, so I first clicked AccountDetails
     * Delete all wishlists and display message "No more wishlists"
     */
    public void deleteWishList() {
        AccountDetails.click();
        mySleeper(1000);
        WishListTab.click();
        Assert.assertEquals(NewWishlistTable.getText(), "Data");
        try {

            do {
                DeleteWishList.click();
                Alert alert = driver.switchTo().alert();
                alert.accept();
            }
            while (
                    DeleteWishList.isDisplayed()
                    );

        } catch (Exception e) {
            System.out.println("No more wishlists");
        }
    }

    /**
     * This method returns ElementName
     */

    public String getElementNameW() {
        ProductNameW = NameElement.getText();
        return ProductNameW;
    }
}
