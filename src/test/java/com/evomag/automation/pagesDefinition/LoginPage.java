package com.evomag.automation.pagesDefinition;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import static com.evomag.automation.BaseTest.driver;
import static com.evomag.automation.utils.TestUtils.mySleeper;

public class LoginPage {
    @FindBy(how = How.XPATH, using = "//H1[text()='Client Nou']")
    private static WebElement NewClient;

    @FindBy(how = How.XPATH, using = "(//H1[text()='Client Existent'])[1]")
    private static WebElement ExistingClient;

    @FindBy(how = How.XPATH, using = "//INPUT[@id='RegisterClientForm_Gender_0']")
    private static WebElement Gender0;

    @FindBy(how = How.XPATH, using = "//INPUT[@id='RegisterClientForm_Gender_1']")
    private static WebElement Gender1;

    @FindBy(how = How.XPATH, using = "//INPUT[@id='RegisterClientForm_FullName']")
    private static WebElement NumePrenume;

    @FindBy(how = How.XPATH, using = "//INPUT[@id='LoginClientForm_Email']")
    private static WebElement Email;

    @FindBy(how = How.XPATH, using = "//INPUT[@id='LoginClientForm_Password']")
    private static WebElement Password;

    @FindBy(how = How.XPATH, using = "//INPUT[@onclick='login_from_cart()']")
    private static WebElement LoginButton;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div[1]/div[2]/div/div[1]")
    private static WebElement ErrorLoginPass;


    /**
     * This method types user and password and push login button
     */

    public static void LoginWithUser(String E, String P) {
        Email.click();
        Email.sendKeys(E);
        Assert.assertEquals(Email.getAttribute("value"), E);
        Password.click();
        Password.sendKeys(P);
        Assert.assertEquals(Password.getAttribute("value"), P);
        LoginButton.click();
        mySleeper(1000);

    }

    /**
     * This method verifies if the error for wrong user and pass is displayed correctly
     */


    public void ValidateErrorWrongUser() {
        ErrorLoginPass.isDisplayed();
        Assert.assertEquals(ErrorLoginPass.getText(), "Parola incorecta! Reincercati sau folositi linkul de recuperare a parolei.");

    }

    /**
     * This method verifies if the error for no user and pass is displayed correctly
     */
    public void ValidateErrorNoUserPass() {
        Alert alert = driver.switchTo().alert();
        Assert.assertEquals(alert.getText(), "Va rugam sa completati adresa de mail si parola.");
        alert.accept();

    }
}
