package com.evomag.automation.pagesDefinition;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import static com.evomag.automation.utils.TestUtils.mySleeper;

public class CampaignPage {
    @FindBy(how = How.XPATH, using = "//A[@href='https://www.evomag.ro'][text()='Inapoi in site']")
    private static WebElement BacktoSite;


    /**
     * This method verifies if a promotion is active an d press the back button to close it
     */
    public void BacktoSitePush() {
        mySleeper(1000);
        try {
            BacktoSite.click();
        } catch (Exception ex) {
            System.out.println("No back Buton");


        }
        mySleeper();
    }
}
