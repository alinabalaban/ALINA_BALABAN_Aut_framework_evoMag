package com.evomag.automation.pagesDefinition;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import static com.evomag.automation.utils.TestUtils.mySleeper;

public class CartPage {
    @FindBy(how = How.CLASS_NAME, using = "cart_product_price_cell")
    private static WebElement PriceCartElement;

    @FindBy(how = How.XPATH, using = "//A[@class='sterge_tab'][text()='sterge']")
    private static WebElement DeleteFrom;

    @FindBy(how = How.XPATH, using = "//DIV[@class='error_cart_empty'][text()='Nu aveti produse in cos!']")
    private static WebElement EmptyCart;


    /**
     * This method verifies that at least one element exists in cart and delete it
     * After that checks if the cart is empty
     */
    public void DeleteElements() {
        DeleteFrom.isDisplayed();
        DeleteFrom.click();
        mySleeper(1000);
        if (EmptyCart.isDisplayed() == true) {
            Assert.assertEquals(EmptyCart.getText(), "Nu aveti produse in cos!");
            System.out.println("The cart is epmty");
        } else {
            System.out.println("The cart is not epmty");
        }
        mySleeper(1000);
    }

    /**
     * This method verifies if the cart is empty, and return the state
     */
    public void CheckCart() {
        if (EmptyCart.isDisplayed() == true) {
            Assert.assertEquals(EmptyCart.getText(), "Nu aveti produse in cos!");
            System.out.println("The cart is epmty");
        } else {
            System.out.println("The cart is not epmty");
        }
    }

}
