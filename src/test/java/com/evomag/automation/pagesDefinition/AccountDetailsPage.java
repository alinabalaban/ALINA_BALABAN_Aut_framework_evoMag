package com.evomag.automation.pagesDefinition;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import static com.evomag.automation.utils.TestUtils.mySleeper;

public class AccountDetailsPage {

    @FindBy(how = How.XPATH, using = "//INPUT[@id='Partner_GenderId_0']")
    private static WebElement Mr;

    @FindBy(how = How.XPATH, using = "//INPUT[@id='Partner_GenderId_1']")
    private static WebElement Mrs;

    @FindBy(how = How.XPATH, using = "//INPUT[@id='Partner_Name']")
    private static WebElement Name;

    @FindBy(how = How.XPATH, using = "//INPUT[@id='Partner_Email']")
    private static WebElement EmailAdress;

    @FindBy(how = How.XPATH, using = "//INPUT[@id='checkCNP']")
    private static WebElement CNP;

    @FindBy(how = How.XPATH, using = "//INPUT[@id='Partner_Phone']")
    private static WebElement Phone;

    @FindBy(how = How.XPATH, using = "//SELECT[@id='ziua']")
    private static WebElement BirthDay;

    @FindBy(how = How.XPATH, using = "//SELECT[@id='luna']")
    private static WebElement BirthMonth;

    @FindBy(how = How.XPATH, using = "//SELECT[@id='anul']")
    private static WebElement BirthYear;

    @FindBy(how = How.XPATH, using = "//INPUT[@id='newsletter']")
    private static WebElement NewsLetter;

    @FindBy(how = How.XPATH, using = "//INPUT[@id='changePassword']")
    private static WebElement Password;

    @FindBy(how = How.XPATH, using = "//A[@id='addAddress']]")
    private static WebElement AddAddress;

    @FindBy(how = How.XPATH, using = "//SELECT[@id='PartnerAddress_County']")
    private static WebElement County;

    @FindBy(how = How.XPATH, using = "//SELECT[@id='PartnerAddress_CityId']")
    private static WebElement CityId;

    @FindBy(how = How.XPATH, using = "//TEXTAREA[@id='PartnerAddress_Address']")
    private static WebElement PartnerAddress;


    @FindBy(how = How.XPATH, using = "//A[@id='save']")
    private static WebElement Save;

    @FindBy(how = How.XPATH, using = " //A[@id='hideAddress']")
    private static WebElement CancelAddress;

    @FindBy(how = How.XPATH, using = "(//H1[text()='Detalii cont'])[1]")
    private static WebElement PageTitle;



    /**
     * This method verifies user account details
     * Display a message "Details are not As expected" if details are not as expected
     * Display a message "Details are As expected"  if details are  as expected
     */
    public void verifyDetails() {
        mySleeper();
        Assert.assertEquals(PageTitle.getText(), "Detalii cont");
        try {
            Assert.assertEquals(Name.getAttribute("value"), "alina balaban");
            Assert.assertEquals(EmailAdress.getAttribute("value"), "abalaban@oberospm.com");
            Assert.assertEquals(CNP.getAttribute("value"), "2920327522958");
            Assert.assertEquals(Phone.getAttribute("value"), "07655454455444");
            System.out.println("Details are As expected");

        } catch (AssertionError e) {
            System.out.println("Details are not As expected ");
            Name.click();
            Name.clear();
            Name.sendKeys("alina balaban");
            EmailAdress.click();
            EmailAdress.clear();
            EmailAdress.sendKeys("abalaban@oberospm.com");
            CNP.click();
            CNP.clear();
            CNP.sendKeys("2920327522958");
            Phone.click();
            Phone.clear();
            Phone.sendKeys("07655454455444");
            Save.click();
        }
        mySleeper();
    }


}
