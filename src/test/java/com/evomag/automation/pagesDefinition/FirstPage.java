package com.evomag.automation.pagesDefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import static com.evomag.automation.BaseTest.driver;
import static com.evomag.automation.utils.TestUtils.mySleeper;

public class FirstPage {
    @FindBy(how = How.XPATH, using = "//EM[text()='\n" + "Login']/self::EM")
    private static WebElement LoginButton;


    @FindBy(how = How.XPATH, using = ".//*[@id='personal_header']/div[2]/div[1]/em")
    private static WebElement Logout;

    @FindBy(how = How.XPATH, using = "//A[@rel='nofollow'][text()='Login']/self::A")
    private static WebElement LoginType;

    @FindBy(how = How.XPATH, using = ".//*[@id='personal_header']/div[2]/div[2]/div[13]/a")
    private static WebElement LogOutButtonAction;

    /**
     * This method clicks Login button, and choose login method
     */

    public static void pushLoginButtons() {
        mySleeper(2000);
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("personal_header")));
        /**
         * Verifies that Login Button exists, and is called Login
         * Then click it
         */
        Assert.assertTrue(LoginButton.isDisplayed());
        LoginButton.click();
        /**
         * Verifies that Login type button is displayed
         * Then click it
         */
        Assert.assertTrue(LoginType.isDisplayed());
        LoginType.click();
        /**
         * Verifies that Login buttons are not displayed anymore
         */
        Assert.assertFalse(LoginType.isDisplayed());
        Assert.assertFalse(LoginButton.isDisplayed());
    }

    /**
     * Verifies that Logout button is displayed
     * Push logout button
     * Verifies logout button is not displayed anymore
     */
    public static void Logout(WebDriver driver) {
        Assert.assertTrue(Logout.isDisplayed());
        Actions builder = new Actions(driver);
        builder.moveToElement(Logout).perform();
        LogOutButtonAction.click();
        Assert.assertFalse(Logout.isDisplayed());
        mySleeper(1000);
    }
}
