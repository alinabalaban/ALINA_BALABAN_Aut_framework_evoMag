package com.evomag.automation.pagesDefinition;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class PopupAddedCart {
    @FindBy(how = How.XPATH, using = "//IMG[@class='close']")
    private static WebElement CloseButton;

    @FindBy(how = How.XPATH, using = "//H1[@id='CrossSaleHeader']")
    private static WebElement ConfirmationToCart;

    @FindBy(how = How.XPATH, using = ".//*[@id='CrossSaleProduct']/a[1]")
    private static WebElement CartDetails;

    /**
     * This method verifies that confirmation message is displayed and is correct, after a product is added to cart
     */
    public void ConfirmationCart() {
        Assert.assertTrue(ConfirmationToCart.isDisplayed());
        Assert.assertEquals(ConfirmationToCart.getText(), "Produsul a fost adaugat in cos");
    }

    /**
     * This method verifies that confirmation message is displayed and is correct, after a product is added to cart
     */
    public void CheckCartDetails() {
        Assert.assertEquals(CartDetails.getText(), "Vezi detalii cos");
        CartDetails.click();
    }

}
