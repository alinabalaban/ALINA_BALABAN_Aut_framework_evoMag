package com.evomag.automation.pagesDefinition;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import org.testng.Reporter;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class IphonesPage {
    ExtentReports ex = new ExtentReports("C:\\Rap\\report.html", false);

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div[1]/div/div[2]/div/h1")
    private static WebElement Welcomemess;

    @FindAll({@FindBy(how = How.CLASS_NAME, using = "real_price")})
    public List<WebElement> IphoneItemsPrice;

    @FindAll({@FindBy(how = How.CLASS_NAME, using = "npi_name")})
    private List<WebElement> IphoneItemList;

    /**
     * Verifies that Iphone page was opened, and the welcome message is displayed
     */
    public void ValidateIphonePage() {
        Welcomemess.isDisplayed();
        Assert.assertEquals(Welcomemess.getText(), "Noul iPhone 8 si iPhone 8 plus");
    }


    /**
     * Connect to database and drop table if exists, create it if it doesn't exist and the delete all values if they exist
     * Then isert all item titles and prices in the table (just added as a exercise)
     */
    public void DatabaseConnection() {
        try {
            /**
             * Connection to database
             */

            Reporter.log("BeforeLogin\n");
            ExtentTest t1 = ex.startTest("DB_test");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/siit_aut?useSSL=false&serverTimezone=UTC", "root", "Ritmic201702");
            Statement stm = conn.createStatement();
            /**
             * Drop table from database
             */
            stm.execute("DROP TABLE IF EXISTS Iphones_example;");
            /**
             * Creates table in database
             */
            stm.execute("create table IF NOT EXISTS  Iphones_example (   ID int NOT NULL AUTO_INCREMENT, Name varchar(255), Price varchar(255),  PRIMARY KEY (ID) );");
            /**
             * Delete all values from table
             */
            stm.execute("delete from Iphones_example;");
            /**
             * Insert into table name and price of the products displayed
             */
            for (int i = 0; i < IphoneItemList.size(); i++) {
                String phone = IphoneItemList.get(i).getText().substring(0, IphoneItemList.get(i).getText().indexOf(","));
                String price = IphoneItemsPrice.get(i).getText();
//                System.out.println(price);
//                System.out.println("insert into Iphones_example(Name, price) values (\""+phone +"\","+"\""+price+"\")");
                stm.execute("insert into Iphones_example(Name, price) values (\"" + phone + "\"," + "\"" + price + "\")");
            }
            /**
             * If somenthing went wrong, display trace
             */
        } catch (SQLException sexc) {
            sexc.printStackTrace();
        }

    }

    /**
     * Validate that values from UI, are the same with values from table
     */
    public void validateDB() {
        try {
            /**
             * Connection to database
             */
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/siit_aut?useSSL=false&serverTimezone=UTC", "root", "Ritmic201702");
            Statement stm = conn.createStatement();
            /**
             * Create a list with names from UI
             */
            List<String> IphoneItemListName = new ArrayList<String>();
            for (int i = 0; i < IphoneItemList.size(); i++) {
//                System.out.println(IphoneItemList.get(i).getText().substring( 0, IphoneItemList.get(i).getText().indexOf(",")));
                IphoneItemListName.add(IphoneItemList.get(i).getText().substring(0, IphoneItemList.get(i).getText().indexOf(",")));
            }
            /**
             * Create a list with prices from UI
             */

            List<String> IphoneItemListPrice = new ArrayList<String>();
            for (int i = 0; i < IphoneItemsPrice.size(); i++) {
//                System.out.println(IphoneItemsPrice.get(i).getText());
                IphoneItemListPrice.add(IphoneItemsPrice.get(i).getText());
            }
            /**
             * Create a list with names from Query
             */
            List<String> QueryResults = new ArrayList<String>();
            ResultSet res = stm.executeQuery("Select name from Iphones_example;");
            while (res.next()) {
                String str = res.getString("name");
                QueryResults.add(str);
            }

            /**
             * Create a list with Prices from Query
             */
            List<String> QueryResultsPrice = new ArrayList<String>();
            ResultSet resPr = stm.executeQuery("Select price from Iphones_example;");
            while (resPr.next()) {
                String str = resPr.getString("price");
                QueryResultsPrice.add(str);
            }

            /**
             * Sorting the lists created
             */
            Collections.sort(IphoneItemListName);
            Collections.sort(QueryResults);
            Collections.sort(IphoneItemListPrice);
            Collections.sort(QueryResultsPrice);


            //Verify the lists are equal
            Assert.assertEquals(IphoneItemListName, QueryResults);
            Assert.assertEquals(IphoneItemListPrice, QueryResultsPrice);


            res.close();
            conn.close();
        } catch (SQLException sexc) {
            sexc.printStackTrace();
        }
    }
}
