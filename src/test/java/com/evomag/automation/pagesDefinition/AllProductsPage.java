package com.evomag.automation.pagesDefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static com.evomag.automation.BaseTest.driver;
import static com.evomag.automation.utils.TestUtils.mySleeper;
import static com.evomag.automation.utils.TestUtils.pressBack;


public class AllProductsPage {
    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div/h1[1]")
    public static WebElement TitleComponents;

    @FindBy(how = How.XPATH, using = "//DIV[@class='noResults'][text()='CRITERIILE DE FILTRARE SELECTATE DE DUMNEAVOASTRA NU AU RETURNAT NICI UN REZULTAT!']")
    private static WebElement NoResult;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div[1]/div/div[2]/div[2]/div[6]/div[1]/div/div[6]/div/a/span")
    private static WebElement NoStock;

    @FindAll({@FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div/h1")})
    private static WebElement Searchconfirmation;

    @FindAll({@FindBy(how = How.XPATH, using = "//INPUT[@id='searchString'][2]")})
    private static WebElement SearchText;

    @FindBy(how = How.XPATH, using = ".//*[@id='sortWidget']")
    private static WebElement SortWidget;

    @FindAll({@FindBy(how = How.CLASS_NAME, using = "real_price")})
    public List<WebElement> ItemsPrices;


    @FindAll({@FindBy(how = How.CLASS_NAME, using = "npi_name")})
    private List<WebElement> productItemList;

    @FindAll({@FindBy(how = How.CLASS_NAME, using = "npi_stock")})
    private List<WebElement> ListOfStocks;
    Random rand = new Random();


    /**
     * This method choose from a list, an elements and  sort products Ascending
     */
    public void sortAscending() {
        Select droplist = new Select(SortWidget);
        droplist.selectByVisibleText("Pret crescator");
        mySleeper();
    }

    /**
     * This method choose from a list, an elements and  sort products Descending
     */
    public void sortDescending() {
        Select droplist = new Select(SortWidget);
        droplist.selectByVisibleText("Pret descrescator");
        mySleeper();
    }

    /**
     * This method verifies that products are sorted as selected
     * Type can be ASC or DESC
     */
    public void checkSort(String type) {

        ArrayList<String> obtainedList = new ArrayList<>();
        List<WebElement> elementList = driver.findElements(By.className("real_price"));
        for (WebElement we : elementList) {
            obtainedList.add(we.getText());
        }

        ArrayList<String> sortedList = new ArrayList<>();
        for (String s : obtainedList) {
            sortedList.add(s);
        }

//        for (int i = 0; i < sortedList.size(); i++) {
//            System.out.println(sortedList.get(i));
//        }
//        for (int i = 0; i < obtainedList.size(); i++) {
//            System.out.println(obtainedList.get(i));
//        }
        if (type == "DESC") {
            Collections.sort(sortedList);
            Collections.reverse(sortedList);
        } else {
            Collections.sort(sortedList);
        }
        Assert.assertTrue(sortedList.equals(obtainedList));
    }

    /**
     * This method verifies that in search result page, the title contains standard message and the value searched
     */
    public void checkProductsPage() {
        mySleeper(1000);
        Assert.assertEquals(Searchconfirmation.getText(), "Rezultate cautare pentru '" + SearchText.getAttribute("value") + "'");
        /**
         * Debug steps
         */
//        System.out.println(Searchconfirmation.getText());
//        System.out.println("Rezultate cautare pentru '" + SearchText.getAttribute("value") + "'");
    }

    /**
     * This method clicks on random element from the list resulted when serching
     */
    public void clickOnRandElement() {
        mySleeper();
        /**
         * If the list is empty, "Search returned no element" will be retrieved
         * Else a random number between 0 and lenght of list-1, then click on the product with that position from list
         */
        if (productItemList.size() == 0) {
            NoResult.isDisplayed();
            System.out.println("Search returned no element");
        } else {
            do {
                int max = productItemList.size() - 1;
                int min = 0;
//                System.out.println(min + " plus " + max);
                int randomNum = rand.nextInt((max - min) + 1) + min;
//                System.out.println("Random number is : " + randomNum);
                if (ListOfStocks.get(randomNum).getText().equals("Stoc epuizat")) {
                    mySleeper(1000);
                    System.out.println("No products with stocks");
                    break;

                } else {
                    productItemList.get(randomNum).click();
                    break;

                }

            }
            while (true);

        }
        mySleeper(1000);
    }

    public void clickOnAllElements() {
        /**
         * If the list is empty, "Search returned no element" will be retrieved
         * Else a random number between 0 and lenght of list-1, then click on the product with that position from list
         */
        if (productItemList.size() == 0) {
            NoResult.isDisplayed();
            System.out.println("Search returned no element");
        } else {
            for (int i = 0; i < productItemList.size(); i++) {
                productItemList.get(i).click();
                pressBack();
            }

        }

    }
}

