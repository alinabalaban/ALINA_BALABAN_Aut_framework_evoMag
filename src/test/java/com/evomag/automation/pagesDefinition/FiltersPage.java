package com.evomag.automation.pagesDefinition;

import com.evomag.automation.BaseTest;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import static com.evomag.automation.utils.TestUtils.mySleeper;

public class FiltersPage extends BaseTest {
    @FindBy(how = How.XPATH, using = ".//*[@id='sn-slider-min']")
    private static WebElement PriceSliderMin;

    @FindBy(how = How.XPATH, using = ".//*[@id='sn-slider-max']")
    private static WebElement PriceSliderMax;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div/div[2]/div[1]/div[1]/div[1]")
    private static WebElement PriceRange;


    @FindBy(how = How.XPATH, using = "//*[contains(text(), 'In stoc magazin')]")
    private static WebElement InStock;

    @FindBy(how = How.XPATH, using = "//INPUT[@id='c19jYXRlZ29yeSYjMzRMYXB0b3B1cmkmIzM0']")
    private static WebElement LaptopCheckBox;


    @FindBy(how = How.XPATH, using = "//INPUT[@id='c19zdG9ja19zdGF0dXMmIzM0aW5fc3RvY19tYWdhemluJiMzNA__']")
    private static WebElement InStockCheckBox;

        @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div/div[2]/div[1]/div[1]/div[3]/a")
//    @FindBy(how = How.XPATH, using = " //A[@class='blue_underline'][text()='Sterge filtre']")
    private static WebElement DeleteFilters;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div/div[2]/div[1]/div[1]/div[1]/a")
    private static WebElement DeleteIndividual;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div/div[2]/div[1]/div[1]/h4")
    private static WebElement FiltersTitle;


    /**
     * This method check filters in stock and laptop, then check if they were checked
     * If filters Laptop does not appear after :in stock is checked, then a message is displayed :"Items you want ro filter are not in stock"
     */

    public void selectFilters() {
        try {
            InStock.click();
            mySleeper(1000);
            Assert.assertTrue(InStockCheckBox.isSelected());
            LaptopCheckBox.click();
            mySleeper(1000);
            Assert.assertTrue(LaptopCheckBox.isSelected());
            JavascriptExecutor jse = ((JavascriptExecutor) driver);
        } catch (Exception e) {
            System.out.println("Items you want ro filter are not in stock");
        }


    }

    /**
     * This method adjust the price slider, then verifies the amount was selected as expected
     * Created a single assert as is a double slider for the same filter
     */
    public void setPriceSlider() {
        Actions move = new Actions(driver);
        Action actionmin = move.dragAndDropBy(PriceSliderMin, 20, 0).build();
        Action actionmax = move.dragAndDropBy(PriceSliderMax, -100, 0).build();
        mySleeper(1000);
        actionmin.perform();
        mySleeper(1000);
        actionmax.perform();
        mySleeper(1000);
//        System.out.println(PriceRange.getText());
        Assert.assertEquals(PriceRange.getText(), "Pret: 1539 Lei - 6744 Lei");
    }

    /**
     * This method delete all the filters, one by one
     * Verifies if the title of filtering area was changed
     * After deleting all the filters a message is displayed:"All filters were deleted"
     * Button delete all was not used, as they have a defect, and that button does not work as expected
     */
    public void deleteFilters() {
try {

    while (DeleteIndividual.isDisplayed()) {
        mySleeper(1000);
        DeleteIndividual.click();
        mySleeper(1000);
    }
}
catch (Exception e){
    System.out.println("All filters were deleted");
}
        mySleeper(1000);

        Assert.assertNotEquals(FiltersTitle.getText(), "Caracteristici alese");

    }
}
