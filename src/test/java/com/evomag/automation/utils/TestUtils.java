package com.evomag.automation.utils;

import com.evomag.automation.BaseTest;

public class TestUtils extends BaseTest{

    public static void mySleeper(int millis) {
        try {
            Thread.sleep(millis);
        }
        catch (Exception ex) {

        }
    }

    public static void pressBack(){
        driver.navigate().back();
    }

    public static void mySleeper() {

        mySleeper(500);
    }

}
